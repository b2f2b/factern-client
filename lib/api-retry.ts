import { Api } from './api';
import * as md from './models/index';
import * as enums from './enums';

export class ApiRetry {

    api: Api;
    defaultMaxAttempts: number;
    defaultErrorCallback: Function | undefined;
    defaultSuccessCallback: Function | undefined;

    constructor(apiUrl: string, user: string, operatingAs: string,
        defaultMaxAttempts: number = 5,
        defaultErrorCallback?: Function,
        defaultSuccessCallback?: Function) {

        this.defaultMaxAttempts = defaultMaxAttempts;
        this.api = new Api(apiUrl, user, operatingAs);

        this.defaultErrorCallback = defaultErrorCallback;
        this.defaultSuccessCallback = defaultSuccessCallback;
    }


    public async createEntity(token: string, parentNodeId: string, name: string, domain: string, errorCallback?: Function, successCallBack?: Function, maxAttempts?: number): Promise<md.Description> {

        let func: Promise<md.Fact> = this.api.createEntity(token, parentNodeId, name, domain);
        return this.wrapperPromise(func, maxAttempts, errorCallback, successCallBack);
    }


    public async describe(token: string, nodeId: string, nextToken: string, includeDeleted?: boolean, maxResults?: number, includeSummary?: boolean, errorCallback?: Function, successCallBack?: Function, maxAttempts?: number): Promise<md.Description> {

        let func: Promise<md.Description> = this.api.describe(token, nodeId, nextToken, includeDeleted, maxResults, includeSummary);
        return this.wrapperPromise(func, maxAttempts, errorCallback, successCallBack);
    }


    public async readByTemplate(token: string, nodeId: string, templateId: string, includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.ReadResult> {

        let func: Promise<md.ReadResult> = this.api.readByTemplate(token, nodeId, templateId, includeSummary);
        return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
    }


    public async readByInlineTemplate(token: string, nodeId: string, template: string[], includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.ReadResult> {

        let func: Promise<md.ReadResult> = this.api.readByInlineTemplate(token, nodeId, template, includeSummary);
        return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
    }


    public async writeByTemplate(token: string, nodeId: string, templateId: string, values: any[], includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.Node> {

        let func: Promise<md.Node> = this.api.writeByTemplate(token, nodeId, templateId, values, includeSummary);
        return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
    }


    public async deleteNode(token: string, nodeId: string, includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.Node> {

        let func: Promise<md.Node> = this.api.deleteNode(token, nodeId, includeSummary);
        return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
    }


    public async searchForEntity(token: string, fieldId: string, operator: enums.SearchOperators, term: string, maxResults: number, restrictTo?: string, includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.SearchResult> {

        let func: Promise<md.SearchResult> = this.api.searchForEntity(token, fieldId, operator, term, maxResults, restrictTo, includeSummary);
        return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
    }


    public async searchEntityByNextToken(token: string, nextToken: string, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.SearchResult> {

        let func: Promise<md.SearchResult> = this.api.searchEntityByNextToken(token, nextToken);
        return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
    }


    private async wrapperPromise(func: Promise<any>, maxAttempts: number = this.defaultMaxAttempts,
        errorCallback: Function | undefined = this.defaultErrorCallback,
        successCallback: Function | undefined = this.defaultSuccessCallback): Promise<any> {

        return new Promise<any>(async (resolve, reject) => {

            let error: any;
            let attempt = 1;


            let work = function () {

                if (attempt < maxAttempts && (error && error.statusCode == 500)) {

                    let start: Date = new Date()

                    func.then((result) => {

                        if (successCallback) {

                            let durationMs: number = (new Date().getTime() - start.getTime());
                            successCallback(attempt, result, durationMs);
                        }

                        resolve(result);

                    }).catch((e) => {

                        error = e;

                        if (errorCallback) {

                            let durationMs: number = (new Date().getTime() - start.getTime());
                            errorCallback(attempt, e, durationMs);
                        }
                        attempt++;

                        setTimeout(() => {
                            work();
                        }, 1000 * Math.pow(attempt, 2));

                    });

                } else {

                    if (errorCallback) {
                        errorCallback(attempt, error);
                    }

                    reject(error);
                }
            };

            work();
        });
    }
}


