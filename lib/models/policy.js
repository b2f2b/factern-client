"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Policy {
    constructor(effect, granteeId, applicationId, actions) {
        this.effect = effect;
        this.granteeId = granteeId;
        this.applicationId = applicationId;
        this.actions = actions;
    }
}
exports.Policy = Policy;
