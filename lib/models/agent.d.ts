export interface Agent {
    application: string;
    login: string;
    representing: string;
}
