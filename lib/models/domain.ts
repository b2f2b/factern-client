import * as md from './index';

export interface Domain extends md.Fact {
    addFact: md.url;
    getFact: md.url;
    queryFacts: md.url;
}

