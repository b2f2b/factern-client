import * as md from './index';
export interface Group extends md.Fact {
    memberFactType: string;
    memberIds: string[];
}
