import * as md from './index';
export interface Member extends md.Fact {
    memberId: string;
}
