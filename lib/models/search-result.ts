import * as md from './index';

export interface SearchResult {
    nodes: md.Node[];
    totalResults: number;
    nextToken: string;
}

