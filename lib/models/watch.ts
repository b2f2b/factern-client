
import * as md from './index';

export interface Watch extends md.Fact {
    watchInterfaceId: string;
    targetNodeId: string;
}