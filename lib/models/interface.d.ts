import * as md from './index';
export interface Interface extends md.Fact {
    addData: md.url;
    getData: md.url;
    deleteData: md.url;
}
