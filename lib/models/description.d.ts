export interface Description {
    node: any;
    children: Children;
}
export interface Children {
    nodes?: (null)[] | null;
    totalResults: number;
    nextToken: string;
}
