import * as md from './index';
export interface Mirror extends md.Node {
    destinationNodeId: string;
    sourceNodeId: string;
    templateId: string;
    enabled: boolean;
}
