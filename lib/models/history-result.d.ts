import * as md from './index';
export interface HistoryResult {
    items: HistoryItem[];
}
export interface HistoryItem {
    node: md.Source;
}
