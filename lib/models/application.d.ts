import * as md from './index';
export interface Application extends md.Fact {
    secret: string;
    name: string;
    redirectUris: string[];
}
