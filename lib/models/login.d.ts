import * as md from './index';
export interface Login extends md.Fact {
    status: string;
    dataRootNode: string;
}
