export interface TokenPayment {

    publickey: string;
    signature: string;
    value: number;
}