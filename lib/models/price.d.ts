import * as md from './index';
export interface Price extends md.Fact {
    policy: md.Policy;
    targetNodeId: string;
    priceDetails: md.PriceDetail;
}
