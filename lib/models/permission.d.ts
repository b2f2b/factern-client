import * as md from './index';
export interface Permission extends md.Fact {
    targetNodeId: string;
    policy: md.Policy;
    permissionInterfaceId: string;
}
