import * as md from './index';
export interface Source extends md.Fact {
    fieldId: string;
    storageId: string;
}
