export interface Facts {
    read: number;
    written: number;
}

export interface Gas {
    consumed: number;
    price: number;
}

export interface Cost {
    gas: Gas;
    total: number;
}

export interface Account {
    balance: number;
}

export interface Summary {
    facts: Facts;
    cost: Cost;
    account: Account;
}