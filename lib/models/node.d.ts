import * as md from './index';
export interface Node {
    agent: md.Agent;
    timestamp: number;
    batchId: string;
    nodeId: string;
    parentId: string;
    summary: md.Summary;
    source: md.Source;
}
