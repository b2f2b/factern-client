import * as md from './index';

export interface Fact extends md.Node {
    factType: string;
    deleted: boolean;
    sequenceNumber: number;
}
