import * as md from './index';

export interface ReadItem {
    fieldId: string;
    nodes: md.Node[];
    nodeId: string;
    data: string;
    children: any[];
}
