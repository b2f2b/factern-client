export interface RequestError {
    statusCode: string;
    message: string;
    options: any;
}
