import * as enums from '../enums';

export class Policy {

    constructor(effect: string, granteeId: string, applicationId: string, actions: enums.Actions[]) {

        this.effect = effect;
        this.granteeId = granteeId;
        this.applicationId = applicationId;
        this.actions = actions

    }

    effect: string;
    granteeId: string;
    applicationId: string;
    actions: enums.Actions[];
}