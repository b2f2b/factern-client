import * as md from './index';
export interface Alias extends md.Fact {
    name: string;
    description: string;
}
