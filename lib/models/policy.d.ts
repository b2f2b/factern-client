import * as enums from '../enums';
export declare class Policy {
    constructor(effect: string, granteeId: string, applicationId: string, actions: enums.Actions[]);
    effect: string;
    granteeId: string;
    applicationId: string;
    actions: enums.Actions[];
}
