import * as md from './index';

export interface ReadResult {
    items: Item[];
}

export interface Item {
    readItem: md.ReadItem;
    status: number;
}



