import * as md from './index';
export interface Filter extends md.Fact {
    statements: any;
}
