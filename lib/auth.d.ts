import * as md from './models/index';
export declare class Auth {
    applicationId: string;
    applicationSecret: string;
    ssoUrl: string;
    constructor(applicationId: string, applicationSecret: string, ssoUrl: string);
    getBase64EncodedCredentials(): string;
    getSSOToken(): Promise<md.Token>;
}
