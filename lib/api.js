"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const rp = require("request-promise");
class Api {
    constructor(apiUrl, user, operatingAs) {
        this.apiUrl = apiUrl;
        this.user = user;
        this.operatingAs = operatingAs;
    }
    createLogin(token, email, password, workflow, redirectUri, domainId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createlogin?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'email': email,
                    'password': password,
                    'workflow': workflow,
                    'redirectUri': redirectUri
                },
                resolveWithFullResponse: false
            };
            if (domainId) {
                options.body['domainId'] = domainId;
            }
            if (includeSummary) {
                options.body['includeSummary'] = includeSummary;
            }
            return rp(options);
        });
    }
    createAlias(token, targetNodeId, name, description, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createalias?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'targetNodeId': targetNodeId,
                    'name': name,
                    'description': description
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = includeSummary;
            }
            return rp(options);
        });
    }
    createApplication(token, parentNodeId, redirectUris, name, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createapplication?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId,
                    'redirectUris': redirectUris,
                    'name': name
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = includeSummary;
            }
            return rp(options);
        });
    }
    createBid(token, priceId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createbid?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'priceId': priceId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = includeSummary;
            }
            return rp(options);
        });
    }
    createDomain(token, parentNodeId, name, addFactUrl, getFactUrl, QueryFactsUrl, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createdomain?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId,
                    'addFact': {
                        'url': addFactUrl
                    },
                    'getFact': {
                        'url': getFactUrl
                    },
                    'queryFacts': {
                        'url': QueryFactsUrl
                    },
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = includeSummary;
            }
            return rp(options);
        });
    }
    createEntity(token, parentNodeId, name, domain) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createentity?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId
                },
                resolveWithFullResponse: false
            };
            if (name) {
                options.body['name'] = name;
            }
            if (domain) {
                options.body['domain'] = domain;
            }
            return rp(options);
        });
    }
    createField(token, parentNodeId, name, branch, uniqueByParent, searchable, description, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createfield?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId,
                    'name': name,
                    'uniqueByParent': uniqueByParent,
                    'searchable': searchable
                },
                resolveWithFullResponse: false
            };
            if (description) {
                options.body['description'] = description;
            }
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createFilter(token, parentNodeId, statements, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createfilter?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId,
                    'statements': statements
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createGroup(token, parentNodeId, name, memberFactType, memberIds, description, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/creategroup?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId,
                    'name': name,
                    'memberFactType': memberFactType,
                    'memberIds': memberIds
                },
                resolveWithFullResponse: false
            };
            if (description) {
                options.body['description'] = description;
            }
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createInformation(token, parentNodeId, fieldId, data, storageId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createinformation?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId,
                    'fieldId': fieldId,
                    'data': data
                },
                resolveWithFullResponse: false
            };
            if (storageId) {
                options.body['storageId'] = storageId;
            }
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createInterface(token, parentNodeId, name, addDataUrl, getDataUrl, deleteDataUrl, description) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createinterface?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId,
                    'name': name,
                    'description': description,
                    'addData': {
                        'url': addDataUrl
                    },
                    'getData': {
                        'url': getDataUrl
                    },
                    'deleteData': {
                        'url': deleteDataUrl
                    },
                },
                resolveWithFullResponse: false
            };
            return rp(options);
        });
    }
    createLabelList(token, name, members, description, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createlabellist?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'name': name,
                    'members': members
                },
                resolveWithFullResponse: false
            };
            if (description) {
                options.body['description'] = description;
            }
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createMember(token, parentNodeId, memberId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createmember?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId,
                    'memberId': memberId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createMirror(token, sourceNodeId, destinationNodeId, templateId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createmirroruser=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'sourceNodeId': sourceNodeId,
                    'destinationNodeId': destinationNodeId,
                    'templateId': templateId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createPermission(token, targetNodeId, policy, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/permission?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'targetNodeId': targetNodeId,
                    'policy': policy
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createPrice(token, targetNodeId, policy, type, priceDetails, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createprice?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'targetNodeId': targetNodeId,
                    'policy': policy,
                    'type': type,
                    'priceDetails': priceDetails
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createScope(token, parentNodeId, name, templateIds, filterIds, description, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createscope?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'parentId': parentNodeId,
                    'name': name,
                    'templateIds': templateIds,
                    'filterIds': filterIds
                },
                resolveWithFullResponse: false
            };
            if (description) {
                options.body['description'] = description;
            }
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createStatement(token, targetNodeId, actionId, actionQualifierId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/createstatement?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'targetNodeid': targetNodeId,
                    'actionId': actionId,
                    'actionQualifierId': actionQualifierId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    createWatchTrigger(token, targetNodeId, watchInterfaceId) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/watch?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'targetNodeId': targetNodeId,
                    'watchInterfaceId': watchInterfaceId
                },
                resolveWithFullResponse: false
            };
            return rp(options);
        });
    }
    resetLogin(token, nodeId, oldPassword, newPassword, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/resetlogin?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId,
                    'oldPassword': oldPassword,
                    'newPassword': newPassword
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = includeSummary;
            }
            return rp(options);
        });
    }
    resetApplication(token, nodeId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/resetapplication?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = includeSummary;
            }
            return rp(options);
        });
    }
    label(token, targetNodeId, labelId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/label?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'targetNodeId': targetNodeId,
                    'labelId': labelId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    readByTemplate(token, nodeId, templateId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.read(token, nodeId, templateId, null, null, includeSummary);
        });
    }
    readByInlineTemplate(token, nodeId, template, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.read(token, nodeId, undefined, template, undefined, includeSummary);
        });
    }
    read(token, nodeId, templateId, template, document, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/read?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId,
                },
                resolveWithFullResponse: false
            };
            if (templateId) {
                options.body['templateId'] = templateId;
            }
            if (template) {
                options.body['template'] = template;
            }
            if (document) {
                options.body['document'] = document;
            }
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    readInformation(token, nodeId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/readinformation?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId,
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    writeByTemplate(token, nodeId, templateId, values, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.write(token, nodeId, values, templateId, undefined, undefined, undefined, undefined, includeSummary);
        });
    }
    writeByDocument(token, nodeId, document, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.write(token, nodeId, undefined, undefined, undefined, document, undefined, undefined, includeSummary);
        });
    }
    writeByInlineTemplate(token, nodeId, template, values, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.write(token, nodeId, values, undefined, template, undefined, undefined, undefined, includeSummary);
        });
    }
    write(token, nodeId, values, templateId, template, document, sourceNodeId, defaultStorageId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/write?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId
                },
                resolveWithFullResponse: false
            };
            if (values) {
                options.body['values'] = values;
            }
            if (templateId) {
                options.body['templateId'] = templateId;
            }
            if (template) {
                options.body['template'] = template;
            }
            if (document) {
                options.body['document'] = document;
            }
            if (sourceNodeId) {
                options.body['sourceNodeId'] = sourceNodeId;
            }
            if (defaultStorageId) {
                options.body['defaultStorageId'] = defaultStorageId;
            }
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    describe(token, nodeId, nextToken, includeDeleted, maxResults = 100, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/describe?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId,
                    'listChildren': {
                        'includeDeleted': includeDeleted,
                        'maxResults': maxResults
                    }
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            if (nextToken) {
                options.body.listChildren['nextToken'] = nextToken;
            }
            return rp(options);
        });
    }
    history(token, nodeId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/history?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    settleAccount(tokenPayment) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/settleaccount?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: {
                    'tokenPayment': tokenPayment
                },
                resolveWithFullResponse: false
            };
            return rp(options);
        });
    }
    requestPermission(token, nodeId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/requestpermission?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = includeSummary;
            }
            return rp(options);
        });
    }
    replaceInformation(token, nodeId, storageIntefaceId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/replaceInformation?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId
                },
                resolveWithFullResponse: false
            };
            if (storageIntefaceId) {
                options.body['storageIntefaceId'] = storageIntefaceId;
            }
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    deleteNode(token, nodeId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/deletenode?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    deleteStatement(token, statementId, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/deletestatement?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'statementId': statementId
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    updateStatus(token, nodeId, status, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/updatestatus?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId,
                    'status': status
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    obliterateNode(token, nodeId) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/obliterate?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nodeId': nodeId
                },
                resolveWithFullResponse: false
            };
            return rp(options);
        });
    }
    searchForEntity(token, fieldId, operator, term, maxResults = 1000, restrictTo, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/searchentity?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'content-type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'fieldId': fieldId,
                    'operator': operator,
                    'term': term,
                    'maxResults': maxResults
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            if (restrictTo) {
                options.body['restrictTo'] = restrictTo;
            }
            return rp(options);
        });
    }
    searchForField(token, property, operator, term, maxResults, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/searchfield?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'content-type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'property': property,
                    'operator': operator,
                    'term': term,
                    'maxResults': maxResults
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    searchForAlias(token, name, includeSummary) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/searchalias?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'content-type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'name': name
                },
                resolveWithFullResponse: false
            };
            if (includeSummary) {
                options.body['includeSummary'] = true;
            }
            return rp(options);
        });
    }
    searchEntityByNextToken(token, nextToken) {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                method: 'POST',
                json: true,
                uri: `${this.apiUrl}/searchentity?user=${this.user}&operatingAs=${this.operatingAs}`,
                headers: {
                    'content-type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: {
                    'nextToken': nextToken
                },
                resolveWithFullResponse: false
            };
            return rp(options);
        });
    }
}
exports.Api = Api;
