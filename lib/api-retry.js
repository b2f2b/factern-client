"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_1 = require("./api");
class ApiRetry {
    constructor(apiUrl, user, operatingAs, defaultMaxAttempts = 5, defaultErrorCallback, defaultSuccessCallback) {
        this.defaultMaxAttempts = defaultMaxAttempts;
        this.api = new api_1.Api(apiUrl, user, operatingAs);
        this.defaultErrorCallback = defaultErrorCallback;
        this.defaultSuccessCallback = defaultSuccessCallback;
    }
    createEntity(token, parentNodeId, name, domain, errorCallback, successCallBack, maxAttempts) {
        return __awaiter(this, void 0, void 0, function* () {
            let func = this.api.createEntity(token, parentNodeId, name, domain);
            return this.wrapperPromise(func, maxAttempts, errorCallback, successCallBack);
        });
    }
    describe(token, nodeId, nextToken, includeDeleted, maxResults, includeSummary, errorCallback, successCallBack, maxAttempts) {
        return __awaiter(this, void 0, void 0, function* () {
            let func = this.api.describe(token, nodeId, nextToken, includeDeleted, maxResults, includeSummary);
            return this.wrapperPromise(func, maxAttempts, errorCallback, successCallBack);
        });
    }
    readByTemplate(token, nodeId, templateId, includeSummary, errorCallback, successCallback, maxAttempts) {
        return __awaiter(this, void 0, void 0, function* () {
            let func = this.api.readByTemplate(token, nodeId, templateId, includeSummary);
            return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
        });
    }
    readByInlineTemplate(token, nodeId, template, includeSummary, errorCallback, successCallback, maxAttempts) {
        return __awaiter(this, void 0, void 0, function* () {
            let func = this.api.readByInlineTemplate(token, nodeId, template, includeSummary);
            return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
        });
    }
    writeByTemplate(token, nodeId, templateId, values, includeSummary, errorCallback, successCallback, maxAttempts) {
        return __awaiter(this, void 0, void 0, function* () {
            let func = this.api.writeByTemplate(token, nodeId, templateId, values, includeSummary);
            return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
        });
    }
    deleteNode(token, nodeId, includeSummary, errorCallback, successCallback, maxAttempts) {
        return __awaiter(this, void 0, void 0, function* () {
            let func = this.api.deleteNode(token, nodeId, includeSummary);
            return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
        });
    }
    searchForEntity(token, fieldId, operator, term, maxResults, restrictTo, includeSummary, errorCallback, successCallback, maxAttempts) {
        return __awaiter(this, void 0, void 0, function* () {
            let func = this.api.searchForEntity(token, fieldId, operator, term, maxResults, restrictTo, includeSummary);
            return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
        });
    }
    searchEntityByNextToken(token, nextToken, errorCallback, successCallback, maxAttempts) {
        return __awaiter(this, void 0, void 0, function* () {
            let func = this.api.searchEntityByNextToken(token, nextToken);
            return this.wrapperPromise(func, maxAttempts, errorCallback, successCallback);
        });
    }
    wrapperPromise(func, maxAttempts = this.defaultMaxAttempts, errorCallback = this.defaultErrorCallback, successCallback = this.defaultSuccessCallback) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                let error;
                let attempt = 1;
                let work = function () {
                    if (attempt < maxAttempts && (error && error.statusCode == 500)) {
                        let start = new Date();
                        func.then((result) => {
                            if (successCallback) {
                                let durationMs = (new Date().getTime() - start.getTime());
                                successCallback(attempt, result, durationMs);
                            }
                            resolve(result);
                        }).catch((e) => {
                            error = e;
                            if (errorCallback) {
                                let durationMs = (new Date().getTime() - start.getTime());
                                errorCallback(attempt, e, durationMs);
                            }
                            attempt++;
                            setTimeout(() => {
                                work();
                            }, 1000 * Math.pow(attempt, 2));
                        });
                    }
                    else {
                        if (errorCallback) {
                            errorCallback(attempt, error);
                        }
                        reject(error);
                    }
                };
                work();
            }));
        });
    }
}
exports.ApiRetry = ApiRetry;
