import { Api } from './api';
import * as md from './models/index';
import * as enums from './enums';
export declare class ApiRetry {
    api: Api;
    defaultMaxAttempts: number;
    defaultErrorCallback: Function | undefined;
    defaultSuccessCallback: Function | undefined;
    constructor(apiUrl: string, user: string, operatingAs: string, defaultMaxAttempts?: number, defaultErrorCallback?: Function, defaultSuccessCallback?: Function);
    createEntity(token: string, parentNodeId: string, name: string, domain: string, errorCallback?: Function, successCallBack?: Function, maxAttempts?: number): Promise<md.Description>;
    describe(token: string, nodeId: string, nextToken: string, includeDeleted?: boolean, maxResults?: number, includeSummary?: boolean, errorCallback?: Function, successCallBack?: Function, maxAttempts?: number): Promise<md.Description>;
    readByTemplate(token: string, nodeId: string, templateId: string, includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.ReadResult>;
    readByInlineTemplate(token: string, nodeId: string, template: string[], includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.ReadResult>;
    writeByTemplate(token: string, nodeId: string, templateId: string, values: any[], includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.Node>;
    deleteNode(token: string, nodeId: string, includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.Node>;
    searchForEntity(token: string, fieldId: string, operator: enums.SearchOperators, term: string, maxResults: number, restrictTo?: string, includeSummary?: boolean, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.SearchResult>;
    searchEntityByNextToken(token: string, nextToken: string, errorCallback?: Function, successCallback?: Function, maxAttempts?: number): Promise<md.SearchResult>;
    private wrapperPromise(func, maxAttempts?, errorCallback?, successCallback?);
}
