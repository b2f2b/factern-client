export enum SearchOperators {
    equals = 'equals',
    startsWith = 'startsWith',
    contains = 'contains'
}


export enum Statuses {
    enabled = 'enabled',
    disabled = 'disabled'
}


export enum Actions {
    read = 'Read',
    act = 'Act',
    Application = 'Application'
}