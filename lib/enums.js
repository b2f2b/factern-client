"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SearchOperators;
(function (SearchOperators) {
    SearchOperators["equals"] = "equals";
    SearchOperators["startsWith"] = "startsWith";
    SearchOperators["contains"] = "contains";
})(SearchOperators = exports.SearchOperators || (exports.SearchOperators = {}));
var Statuses;
(function (Statuses) {
    Statuses["enabled"] = "enabled";
    Statuses["disabled"] = "disabled";
})(Statuses = exports.Statuses || (exports.Statuses = {}));
var Actions;
(function (Actions) {
    Actions["read"] = "Read";
    Actions["act"] = "Act";
    Actions["Application"] = "Application";
})(Actions = exports.Actions || (exports.Actions = {}));
