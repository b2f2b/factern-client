"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const rp = require("request-promise");
class Auth {
    constructor(applicationId, applicationSecret, ssoUrl) {
        this.applicationId = applicationId;
        this.applicationSecret = applicationSecret;
        this.ssoUrl = ssoUrl;
    }
    getBase64EncodedCredentials() {
        let c = `${this.applicationId}:${this.applicationSecret}`;
        return new Buffer(c).toString('base64');
    }
    getSSOToken() {
        return __awaiter(this, void 0, void 0, function* () {
            let encodedCredentials = this.getBase64EncodedCredentials();
            const options = {
                method: 'POST',
                json: true,
                uri: this.ssoUrl,
                headers: {
                    'content-type': 'application/x-www-form-urlencoded',
                    'accept': 'application/json',
                    'Authorization': `Basic ${encodedCredentials}`
                },
                body: 'grant_type=client_credentials'
            };
            return rp(options);
        });
    }
}
exports.Auth = Auth;
