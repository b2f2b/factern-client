import * as rp from 'request-promise';
import * as md from './models/index';

export class Auth {

    applicationId: string;
    applicationSecret: string;
    ssoUrl: string;

    constructor(applicationId: string, applicationSecret: string, ssoUrl: string) {

        this.applicationId = applicationId;
        this.applicationSecret = applicationSecret;
        this.ssoUrl = ssoUrl;
    }

    public getBase64EncodedCredentials(): string {

        let c: string = `${this.applicationId}:${this.applicationSecret}`;
        return new Buffer(c).toString('base64');
    }

    public async getSSOToken(): Promise<md.Token> {

        let encodedCredentials: string = this.getBase64EncodedCredentials();

        const options = {
            method: 'POST',
            json: true,
            uri: this.ssoUrl,
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'accept': 'application/json',
                'Authorization': `Basic ${encodedCredentials}`
            },
            body: 'grant_type=client_credentials'
        };

        return rp(options);
    }
}
