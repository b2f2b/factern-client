export declare enum SearchOperators {
    equals = "equals",
    startsWith = "startsWith",
    contains = "contains",
}
export declare enum Statuses {
    enabled = "enabled",
    disabled = "disabled",
}
export declare enum Actions {
    read = "Read",
    act = "Act",
    Application = "Application",
}
