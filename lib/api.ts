import * as rp from 'request-promise';
import * as md from './models/index';
import * as enums from './enums';

export class Api {

    apiUrl: string;
    user: string;
    operatingAs: string;

    constructor(apiUrl: string, user: string, operatingAs: string) {

        this.apiUrl = apiUrl;
        this.user = user;
        this.operatingAs = operatingAs;
    }


    public async createLogin(token: string, email: string, password: string, workflow: string, redirectUri: string, domainId?: string | null, includeSummary?: boolean): Promise<md.Login> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createlogin?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'email': email,
                'password': password,
                'workflow': workflow,
                'redirectUri': redirectUri
            },
            resolveWithFullResponse: false
        };

        if (domainId) {
            options.body['domainId'] = domainId;
        }

        if (includeSummary) {
            options.body['includeSummary'] = includeSummary;
        }

        return rp(options);
    }


    public async createAlias(token: string, targetNodeId: string, name: string, description: string, includeSummary?: boolean): Promise<md.Alias> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createalias?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'targetNodeId': targetNodeId,
                'name': name,
                'description': description
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = includeSummary;
        }

        return rp(options);
    }


    public async createApplication(token: string, parentNodeId: string, redirectUris: string[], name: string, includeSummary?: boolean): Promise<md.Application> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createapplication?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId,
                'redirectUris': redirectUris,
                'name': name
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = includeSummary;
        }

        return rp(options);
    }


    public async createBid(token: string, priceId: string, includeSummary?: boolean): Promise<md.Node> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createbid?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'priceId': priceId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = includeSummary;
        }

        return rp(options);
    }


    public async createDomain(token: string, parentNodeId: string, name: string, addFactUrl: string, getFactUrl: string, QueryFactsUrl: string, includeSummary?: boolean): Promise<md.Domain> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createdomain?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId,
                'addFact': {
                    'url': addFactUrl
                },
                'getFact': {
                    'url': getFactUrl
                },
                'queryFacts': {
                    'url': QueryFactsUrl
                },
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = includeSummary;
        }

        return rp(options);
    }


    public async createEntity(token: string, parentNodeId: string, name?: string, domain?: string): Promise<md.Fact> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createentity?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId
            },
            resolveWithFullResponse: false
        };

        if (name) {
            options.body['name'] = name;
        }

        if (domain) {
            options.body['domain'] = domain;
        }

        return rp(options);

    }


    public async createField(token: string, parentNodeId: string, name: string, branch: boolean, uniqueByParent: boolean, searchable: boolean, description: string | null, includeSummary?: boolean): Promise<md.Fact> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createfield?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId,
                'name': name,
                'uniqueByParent': uniqueByParent,
                'searchable': searchable
            },
            resolveWithFullResponse: false
        };

        if (description) {
            options.body['description'] = description;
        }

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }

    public async createFilter(token: string, parentNodeId: string, statements: any, includeSummary?: boolean): Promise<md.Filter> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createfilter?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId,
                'statements': statements
            },
            resolveWithFullResponse: false
        };


        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createGroup(token: string, parentNodeId: string, name: string, memberFactType: string, memberIds: string[], description: string | null, includeSummary?: boolean): Promise<md.Group> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/creategroup?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId,
                'name': name,
                'memberFactType': memberFactType,
                'memberIds': memberIds
            },
            resolveWithFullResponse: false
        };

        if (description) {
            options.body['description'] = description;
        }

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createInformation(token: string, parentNodeId: string, fieldId: string, data: string, storageId: string | null, includeSummary?: boolean): Promise<md.Fact> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createinformation?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId,
                'fieldId': fieldId,
                'data': data
            },
            resolveWithFullResponse: false
        };

        if (storageId) {
            options.body['storageId'] = storageId;
        }

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createInterface(token: string, parentNodeId: string, name: string, addDataUrl: string, getDataUrl: string, deleteDataUrl: string, description: string | null): Promise<md.Interface> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createinterface?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId,
                'name': name,
                'description': description,
                'addData': {
                    'url': addDataUrl
                },
                'getData': {
                    'url': getDataUrl
                },
                'deleteData': {
                    'url': deleteDataUrl
                },
            },
            resolveWithFullResponse: false
        };

        return rp(options);
    }


    public async createLabelList(token: string, name: string, members: string[], description: string | null, includeSummary?: boolean): Promise<md.Fact> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createlabellist?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'name': name,
                'members': members
            },
            resolveWithFullResponse: false
        };

        if (description) {
            options.body['description'] = description;
        }

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createMember(token: string, parentNodeId: string, memberId: string, includeSummary?: boolean): Promise<md.Fact> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createmember?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId,
                'memberId': memberId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createMirror(token: string, sourceNodeId: string, destinationNodeId: string, templateId: string, includeSummary?: boolean): Promise<md.Mirror> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createmirroruser=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'sourceNodeId': sourceNodeId,
                'destinationNodeId': destinationNodeId,
                'templateId': templateId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createPermission(token: string, targetNodeId: string, policy: md.Policy, includeSummary?: boolean): Promise<md.Permission> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/permission?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'targetNodeId': targetNodeId,
                'policy': policy
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createPrice(token: string, targetNodeId: string, policy: md.Policy, type: string, priceDetails: md.PriceDetail, includeSummary?: boolean): Promise<md.Price> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createprice?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'targetNodeId': targetNodeId,
                'policy': policy,
                'type': type,
                'priceDetails': priceDetails
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createScope(token: string, parentNodeId: string, name: string, templateIds: string[], filterIds: string[], description: string | null, includeSummary?: boolean): Promise<md.Fact> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createscope?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'parentId': parentNodeId,
                'name': name,
                'templateIds': templateIds,
                'filterIds': filterIds
            },
            resolveWithFullResponse: false
        };

        if (description) {
            options.body['description'] = description;
        }

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createStatement(token: string, targetNodeId: string, actionId: string, actionQualifierId: string, includeSummary?: boolean): Promise<md.Node> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/createstatement?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'targetNodeid': targetNodeId,
                'actionId': actionId,
                'actionQualifierId': actionQualifierId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async createWatchTrigger(token: string, targetNodeId: string, watchInterfaceId: string): Promise<md.Watch> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/watch?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'targetNodeId': targetNodeId,
                'watchInterfaceId': watchInterfaceId
            },
            resolveWithFullResponse: false
        };

        return rp(options);
    }


    public async resetLogin(token: string, nodeId: string, oldPassword: string, newPassword: string, includeSummary?: boolean): Promise<md.Node> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/resetlogin?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId,
                'oldPassword': oldPassword,
                'newPassword': newPassword
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = includeSummary;
        }

        return rp(options);
    }


    public async resetApplication(token: string, nodeId: string, includeSummary?: boolean): Promise<md.Application> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/resetapplication?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = includeSummary;
        }

        return rp(options);
    }


    public async label(token: string, targetNodeId: string, labelId: string, includeSummary?: boolean): Promise<md.Fact> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/label?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'targetNodeId': targetNodeId,
                'labelId': labelId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);

    }


    public async readByTemplate(token: string, nodeId: string, templateId: string, includeSummary?: boolean): Promise<md.ReadResult> {
        return this.read(token, nodeId, templateId, null, null, includeSummary);
    }


    public async readByInlineTemplate(token: string, nodeId: string, template: string[], includeSummary?: boolean): Promise<md.ReadResult> {
        return this.read(token, nodeId, undefined, template, undefined, includeSummary);
    }


    private async read(token: string, nodeId: string, templateId?: string, template?: any, document?: any, includeSummary?: boolean): Promise<md.ReadResult> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/read?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId,
            },
            resolveWithFullResponse: false
        };

        if (templateId) {
            options.body['templateId'] = templateId;
        }

        if (template) {
            options.body['template'] = template;
        }

        if (document) {
            options.body['document'] = document;
        }

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async readInformation(token: string, nodeId: string, includeSummary?: boolean): Promise<md.ReadInformationResult> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/readinformation?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId,
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async writeByTemplate(token: string, nodeId: string, templateId: string, values: any[], includeSummary?: boolean): Promise<md.Node> {
        return this.write(token, nodeId, values, templateId, undefined, undefined, undefined, undefined, includeSummary);
    }


    public async writeByDocument(token: string, nodeId: string, document: any, includeSummary?: boolean): Promise<md.Node> {
        return this.write(token, nodeId, undefined, undefined, undefined, document, undefined, undefined, includeSummary);
    }


    public async writeByInlineTemplate(token: string, nodeId: string, template: string[], values: any[], includeSummary?: boolean): Promise<md.Node> {
        return this.write(token, nodeId, values, undefined, template, undefined, undefined, undefined, includeSummary);
    }


    private async write(token: string, nodeId: string, values?: any[], templateId?: string, template?: string[], document?: any, sourceNodeId?: string, defaultStorageId?: string | null, includeSummary?: boolean): Promise<md.Node> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/write?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId
            },
            resolveWithFullResponse: false
        };

        if (values) {
            options.body['values'] = values;
        }

        if (templateId) {
            options.body['templateId'] = templateId;
        }

        if (template) {
            options.body['template'] = template;
        }

        if (document) {
            options.body['document'] = document;
        }

        if (sourceNodeId) {
            options.body['sourceNodeId'] = sourceNodeId;
        }

        if (defaultStorageId) {
            options.body['defaultStorageId'] = defaultStorageId;
        }

        if (includeSummary) {
            options.body['includeSummary'] = true;
        }

        return rp(options);
    }


    public async describe(token: string, nodeId: string, nextToken?: string, includeDeleted?: boolean, maxResults: number = 100, includeSummary?: boolean): Promise<md.Description> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/describe?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId,
                'listChildren': {
                    'includeDeleted': includeDeleted,
                    'maxResults': maxResults
                }
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true
        }

        if (nextToken) {
            options.body.listChildren['nextToken'] = nextToken;
        }

        return rp(options);

    }


    public async history(token: string, nodeId: string, includeSummary?: boolean): Promise<md.HistoryResult> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/history?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true
        }

        return rp(options);

    }


    public async settleAccount(tokenPayment: md.TokenPayment): Promise<md.HistoryResult> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/settleaccount?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: {
                'tokenPayment': tokenPayment
            },
            resolveWithFullResponse: false
        };

        return rp(options);

    }


    public async requestPermission(token: string, nodeId: string, includeSummary?: boolean): Promise<md.Node> {

        const options = {

            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/requestpermission?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = includeSummary;
        }

        return rp(options);
    }


    public async replaceInformation(token: string, nodeId: string, storageIntefaceId?: string, includeSummary?: boolean): Promise<md.Node> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/replaceInformation?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId
            },
            resolveWithFullResponse: false
        };


        if (storageIntefaceId) {
            options.body['storageIntefaceId'] = storageIntefaceId
        }

        if (includeSummary) {
            options.body['includeSummary'] = true
        }

        return rp(options);

    }


    public async deleteNode(token: string, nodeId: string, includeSummary?: boolean): Promise<md.Fact> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/deletenode?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true
        }

        return rp(options);

    }


    public async deleteStatement(token: string, statementId: string, includeSummary?: boolean): Promise<md.Node> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/deletestatement?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'statementId': statementId
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true
        }

        return rp(options);

    }


    public async updateStatus(token: string, nodeId: string, status: enums.Statuses, includeSummary?: boolean): Promise<md.Node> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/updatestatus?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId,
                'status': status
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true
        }

        return rp(options);

    }


    public async obliterateNode(token: string, nodeId: string): Promise<any> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/obliterate?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nodeId': nodeId
            },
            resolveWithFullResponse: false
        };

        return rp(options);

    }


    public async searchForEntity(token: string, fieldId: string, operator: enums.SearchOperators, term: string, maxResults: number = 1000, restrictTo?: string, includeSummary?: boolean): Promise<md.SearchResult> {


        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/searchentity?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'content-type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'fieldId': fieldId,
                'operator': operator,
                'term': term,
                'maxResults': maxResults
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true
        }

        if (restrictTo) {
            options.body['restrictTo'] = restrictTo;
        }

        return rp(options);
    }


    public async searchForField(token: string, property: string, operator: enums.SearchOperators, term: string, maxResults: number, includeSummary?: boolean): Promise<md.SearchResult> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/searchfield?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'content-type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'property': property,
                'operator': operator,
                'term': term,
                'maxResults': maxResults
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true
        }

        return rp(options);
    }


    public async searchForAlias(token: string, name: string, includeSummary?: boolean): Promise<md.SearchResult> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/searchalias?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'content-type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'name': name
            },
            resolveWithFullResponse: false
        };

        if (includeSummary) {
            options.body['includeSummary'] = true
        }

        return rp(options);
    }


    public async searchEntityByNextToken(token: string, nextToken: string): Promise<md.SearchResult> {

        const options = {
            method: 'POST',
            json: true,
            uri: `${this.apiUrl}/searchentity?user=${this.user}&operatingAs=${this.operatingAs}`,
            headers: {
                'content-type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: {
                'nextToken': nextToken
            },
            resolveWithFullResponse: false
        };

        return rp(options);
    }

}
