# Test client

Node client library for connecting to Factern in Typescript

## Installation

This isn't published on NPM yet, however you can install specific versions via url:

npm install --save https://s3.eu-west-2.amazonaws.com/agfete-public-library/factern-client-1.0.2.tgz


## Example usage using typescript async / await

import * as fc from 'factern-client';

const nodeId: string = '000000000434A0475EB9CEDCA142CD35BDDE3131F3F4086C';
const userId: string = 'your user id';
const clientId: string = 'your client application id';
const clientSecret: string = 'your client secret';

let main = (async function () {

    const facternAuth: fc.Auth = new fc.Auth(clientId, clientSecret, 'https://sso.factern.com/auth/token');
    const facternApi: fc.Api = new fc.Api('https://api.factern.com/v2', userId, userId);

    let token: fc.Token = await facternAuth.getSSOToken();

    let describeResult: any = await facternApi.describe(token.access_token, nodeId);

    console.log(describeResult);

})();
